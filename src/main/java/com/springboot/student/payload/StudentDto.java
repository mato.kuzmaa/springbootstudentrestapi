package com.springboot.student.payload;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Set;

@Data
public class StudentDto {
    private long id;

    // name should not be null or empty
    // name should have at least 3 characters
    @NotEmpty
    @Size(min = 3, message = "Student's name should have at least 2 characters.")
    private String name;

    // surname should not be null or empty
    // surname should have at least 4 characters
    @NotEmpty
    @Size(min = 4, message = "Student's surname should have at least 4 characters.")
    private String surname;

    // group should not be null or empty
    @NotEmpty
    private String group;

    private Set<CommentDto> comments;

    private Long categoryId;
}
