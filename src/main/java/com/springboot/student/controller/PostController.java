package com.springboot.student.controller;

import com.springboot.student.payload.PostResponse;
import com.springboot.student.payload.StudentDto;
import com.springboot.student.service.PostService;
import com.springboot.student.utils.AppConstants;
import jakarta.validation.Valid;
import net.sf.jasperreports.engine.JRException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.util.List;

@RestController
@RequestMapping()
public class PostController {

    private PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    // create student post rest api
    // !!!!!!!!!!!!!!!!!!! nefunguje mi to cez postmana s vyuzitim tokena
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/api/v1/posts")
    public ResponseEntity<StudentDto> createStudent(@Valid @RequestBody StudentDto studentDto) {
        return new ResponseEntity<>(postService.createStudent(studentDto), HttpStatus.CREATED);
    }

    // http://localhost:8080/api/posts?pageNo=0&pagesize=10&sortBy=name&sortDir=asc
    // get all students rest api
    @GetMapping("/api/v1/posts")
    public PostResponse getAllStudents(
            @RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
            @RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir)
    {
        return postService.getAllStudents(pageNo, pageSize, sortBy, sortDir);
    }

    // get student by id rest api
    @GetMapping(value = "/api/v1/posts/{id}")                           // preco za pathvariable je - name = "id" ??
    public ResponseEntity<StudentDto> getStudentByIdV1(@PathVariable(name = "id") long id) {
        return ResponseEntity.ok(postService.getStudentById(id));
    }

    // @RequestBody - json request to object
    // update student by id rest api
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/api/v1/posts/{id}")
    public ResponseEntity<StudentDto> updateStudent(@Valid @RequestBody StudentDto studentDto,
                                                    @PathVariable(name = "id") long id) {
        StudentDto postResponse = postService.updateStudent(studentDto, id);
        return ResponseEntity.ok(postResponse);
    }

    // delete student by id rest api
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/api/v1/posts/{id}")
    public ResponseEntity<String> deleteStudentById(@PathVariable(name = "id") long id) {

        postService.deleteStudentById(id);

        return ResponseEntity.ok("Student was successfully deleted.");
    }

    // Generate Jasper Report PDF
    @GetMapping("/report/{format}")
    public String generateReport(@PathVariable String format) throws JRException, FileNotFoundException {
        return postService.exportReport(format);
    }

    // Build Get Posts by Category REST API
    // http://localhost:8080/api/posts/category/3
    @GetMapping("/category/{id}")
    public ResponseEntity<List<StudentDto>> getPostsByCategory(@PathVariable("id") Long categoryId) {
        List<StudentDto> postDtos = postService.getPostsByCategory(categoryId);
        return ResponseEntity.ok(postDtos);
    }
}

























