package com.springboot.student.service;

import com.springboot.student.payload.LoginDto;
import com.springboot.student.payload.RegisterDto;

public interface AuthService {
    String login(LoginDto loginDto);

    String register(RegisterDto registerDto);
}
