package com.springboot.student.service;

import com.springboot.student.payload.PostResponse;
import com.springboot.student.payload.StudentDto;
import net.sf.jasperreports.engine.JRException;

import java.io.FileNotFoundException;
import java.util.List;

public interface PostService {
    StudentDto createStudent(StudentDto studentDto);

    PostResponse getAllStudents(int pageNo, int pageSize, String sortBy, String sortDir);

    StudentDto getStudentById(long id);

    StudentDto updateStudent(StudentDto studentDto, long id);

    void deleteStudentById(long id);

    String exportReport(String reportFormat) throws FileNotFoundException, JRException;

    List<StudentDto> getPostsByCategory(Long categoryId);
}
