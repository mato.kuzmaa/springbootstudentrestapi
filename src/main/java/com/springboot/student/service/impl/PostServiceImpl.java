package com.springboot.student.service.impl;

import com.springboot.student.entity.Category;
import com.springboot.student.entity.Post;
import com.springboot.student.exception.ResourceNotFoundException;
import com.springboot.student.payload.PostResponse;
import com.springboot.student.payload.StudentDto;
import com.springboot.student.repository.CategoryRepository;
import com.springboot.student.repository.PostRepository;
import com.springboot.student.repository.PostRepository2;
import com.springboot.student.service.PostService;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service            // ak tato anotacia znamena pouzitie beany tejto triedy, tak v kt triede je pouzita tato beana
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;

    private ModelMapper mapper;

    private CategoryRepository categoryRepository;
    private final PostRepository2 postRepository2;

    public PostServiceImpl(PostRepository postRepository, ModelMapper mapper, CategoryRepository categoryRepository, PostRepository2 postRepository2) {
        this.postRepository = postRepository;
        this.mapper = mapper;
        this.categoryRepository = categoryRepository;
        this.postRepository2 = postRepository2;
    }

    @Override
    public StudentDto createStudent(StudentDto studentDto) {

        Category category = categoryRepository.findById(studentDto.getCategoryId())
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", studentDto.getCategoryId()));

        // convert DTO to entity - data prichadzajuce DTO do tabulky
        Post post = mapToEntity(studentDto);
        post.setCategory(category);
        Post newPost = postRepository.save(post);

        // convert entity to DTO  - data z sql tabulky do StudentDto, kt sa zobrazia ako vystup pre uzivatela
        StudentDto postResponse = mapToDTO(newPost);
        return postResponse;

    }

    // ascending vzostupne, descending zostupne
    @Override
    public PostResponse getAllStudents(int pageNo, int pageSize, String sortBy, String sortDir) {

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        // create Pageable instance
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);

        Page<Post> posts = postRepository.findAll(pageable);

        // get content for page object
        List<Post> listOfPosts = posts.getContent();

        List<StudentDto> content = listOfPosts.stream().map(post -> mapToDTO(post)).collect(Collectors.toList());

        PostResponse postResponse = new PostResponse();
        postResponse.setContent(content);
        postResponse.setPageNo(posts.getNumber());
        postResponse.setPageSize(posts.getSize());
        postResponse.setTotalElements(posts.getTotalElements());
        postResponse.setTotalPages(posts.getTotalPages());
        postResponse.setLast(posts.isLast());

        return postResponse;
    }

    @Override
    public StudentDto getStudentById(long id) {
        Post post = postRepository.findById(id).
                orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));
        return mapToDTO(post);
    }


    // updateStudent - parametre metody: vkladam noveho studenta / hladam studenta s id, kt idem menit
    @Override
    public StudentDto updateStudent(StudentDto studentDto, long id) {
        // get student by id from database
        Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));

        Category category = categoryRepository.findById(studentDto.getCategoryId())
                        .orElseThrow(() -> new ResourceNotFoundException("Category", "id", studentDto.getCategoryId()));

        post.setName(studentDto.getName());
        post.setSurname(studentDto.getSurname());
        post.setGroup(studentDto.getGroup());
        post.setCategory(category);

        Post updateStudent = postRepository.save(post);
        return mapToDTO(updateStudent);
    }

    @Override
    public void deleteStudentById(long id) {
        // get student by id from database
        Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));

        postRepository.delete(post);
    }


    // convert entity to DTO
    private StudentDto mapToDTO(Post post) {

        StudentDto studentDto = mapper.map(post, StudentDto.class);         // (source, destinationType)

        // StudentDto studentDto = new StudentDto();
        // studentDto.setId(post.getId());
        // studentDto.setName(post.getName());
        // studentDto.setSurname(post.getSurname());
        // studentDto.setGroup(post.getGroup());
        return studentDto;
    }

    // convert DTO to entity
    private Post mapToEntity(StudentDto studentDto) {

        Post post = mapper.map(studentDto, Post.class);                     // (source, destinationType)

        // Post post = new Post();
        // post.setName(studentDto.getName());
        // post.setSurname(studentDto.getSurname());
        // post.setGroup(studentDto.getGroup());
        return post;
    }


    @Override
    public String exportReport(String reportFormat) throws FileNotFoundException, JRException {
        String path = "C:\\Spring Boot data\\Report Studentov";
        List<Post> students = postRepository2.findAll();

        File file = ResourceUtils.getFile("classpath:students.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(students);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "Mato");

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        if(reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "\\students.html");
        }
        if(reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "\\students.pdf");
        }
        return "report generated in path : " + path;
    }

    @Override
    public List<StudentDto> getPostsByCategory(Long categoryId) {

        categoryRepository.findById(categoryId)
                        .orElseThrow(() -> new ResourceNotFoundException("Category", "id", categoryId));

        List<Post> posts = postRepository.findByCategoryId(categoryId);

        return posts.stream().map((post) -> mapToDTO(post))
                .collect(Collectors.toList());
    }
}
