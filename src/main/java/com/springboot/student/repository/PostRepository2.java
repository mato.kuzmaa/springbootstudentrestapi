package com.springboot.student.repository;

import com.springboot.student.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository2 extends JpaRepository<Post, Integer> {
}
